from xbmcswift2 import Plugin
from xbmcswift2 import ListItem
from resources.lib.teliaapi import TeliaApi
from xbmcswift2 import xbmc, xbmcgui

import inputstreamhelper

plugin = Plugin()

ssoToken = plugin.get_setting('sso_token')

telia_api = TeliaApi(plugin = plugin, ssoToken = ssoToken)

PROTOCOL = 'hls'


@plugin.route('/')
def index():
    items = []
    for item in telia_api.get_channel_lists():
        items.append({
            "label": item["title"],
            "path": plugin.url_for("channel_list_channels", channel_list_id = item["id"])
        })
    return items

@plugin.route('/channel_list_channels/<channel_list_id>')
def channel_list_channels(channel_list_id):
    items = []
    channel_ids = telia_api.get_channel_list_channels(channel_list_id)
    channels = telia_api.get_channels()

    for channel_id in channel_ids:
        channel = channels[str(channel_id)]
        items.append({
            "label": channel["title"],
            "path": plugin.url_for("play_channel", channel_id = channel["id"])
        })
    return items


@plugin.route('/play_channel/<channel_id>/')
def play_channel(channel_id):
    # telia_api.get_random_source(channel_id)
    # return []
    helper = inputstreamhelper.Helper(PROTOCOL)
    if helper.check_inputstream():
        play_item = ListItem(label = "Play", path = telia_api.get_random_source(channel_id))
        play_item.add_stream_info('inputstreamaddon', helper.inputstream_addon)
        play_item.add_stream_info('inputstream.adaptive.manifest_type', PROTOCOL)
        plugin.play_video(play_item)

if __name__ == '__main__':
    plugin.run()
