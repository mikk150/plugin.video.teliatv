import time
import xbmc

from xbmcswift2 import Plugin
from resources.lib.teliaapi import TeliaApi

plugin = Plugin()

ssoToken = plugin.get_setting('sso_token')

telia_api = TeliaApi(plugin = plugin, ssoToken = ssoToken)

if __name__ == '__main__':
    monitor = xbmc.Monitor()
    
    while not monitor.abortRequested():
        # Sleep/wait for abort for 15 minutes
        if monitor.waitForAbort(60 * 15):
            # Abort was requested while waiting. We should exit
            break

        telia_api.get_info()