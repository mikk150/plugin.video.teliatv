import requests
import random
import logging

class TeliaApi:
    def __init__(self, plugin, ssoToken):
        self.plugin = plugin
        self.requests = requests.Session()
        self.storage = self.plugin.get_storage('telia_api')

        loginResponse = self.requests.post("https://api.teliatv.ee/dtv-api/3.0/et/authentication/sso", {
            "ssoToken": ssoToken,
            "uuid": self.storage.get('uuid'),
            "hwVersion": "Kodi",
            "ui": "tv-web"
        }).json()
        self.storage.uuid = loginResponse["deviceSerial"]

    def get_info (self):
        return self.requests.get("https://api.teliatv.ee/dtv-api/2.1/et/info?ui=tv-web")

    def get_channels (self):
        return self.requests.get("https://api.teliatv.ee/dtv-api/3.0/et/channel-lists?listClass=tv&ui=tv-web").json()["channels"]
    
    def get_play_channel (self, channel_id):
        return self.requests.post("https://api.teliatv.ee/dtv-api/2.2/et/channel/" + channel_id + "/start_playing").json()

    def get_channel_lists (self):
        return self.requests.get("https://api.teliatv.ee/dtv-api/3.0/et/channel-lists?listClass=tv&ui=tv-web").json()["baseChannelLists"]
    
    def get_channel_list_channels (self, channel_list_id):
        channel_list_positions = self.requests.get("https://api.teliatv.ee/dtv-api/3.0/et/channel-lists?listClass=tv&ui=tv-web").json()["baseChannelListPositions"]

        return channel_list_positions[channel_list_id]

    def get_channel_streams (self, channel_id):
        streams = []
        for channel in self.get_play_channel(channel_id) :
            playables = channel["data"]["context_objects"]["playables"]
            return playables[channel["data"]["playable_id"]]["streams"]
            # print(json.dumps(streams))

            # streams = channel["data"]["context_objects"]["playables"][channel["data"]["playable_id"]]["streams"]
        return streams
    
    def get_channel_sources (self, channel_id):
        streams = self.get_channel_streams(channel_id)
        sources = []
        for stream in streams :
            return stream["sources"]
            sources + stream["sources"]
        return sources

    def get_random_source (self, channel_id):
        sources = self.get_channel_sources(channel_id)
        print(random.choice(sources))
        return random.choice(sources)



